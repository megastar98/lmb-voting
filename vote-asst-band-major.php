<?php
// fetch presidential candidates
$candidates = $bandit->getCandidates(4);
?>
<html lang="en">
<head>
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Raleway&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="index.css"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Assistant Band Major</title>
</head>
<body>
    <header>
        <div id="header">
            <nav ìd="menu-bar" class="nav-wrapper transparent no-shadows flat">
                <div id="band_logo">
                    <a href="home.html" class="brand-logo">
                        <img class="logos" src="nysc.png">
                    </a>
                </div>
            </nav>
        </div>
        <!-- position to vote for -->
        <div class="position">
            <h4>Assistant Band Major</h4>
        </div>

        <!-- candidates to vote for -->
        <div id="candidates" class="container">
            <div class=" row">
            <?php
                if (!empty($candidates)) {
                    foreach ($candidates as $candidate) { ?>
                        <div class="col l3 m6 s12 Candidate ">
                            <div class="carde card">
                                <div class="card-image">
                                    <img src="happy.jpg">
                                </div>
                                <div class="card-content candidatename">
                                    <h6><?= $candidate['full_name'] ?></h6>
                                    <p>Presidential Candidate</p>
                                </div>
                            </div>
                            <button id="<?= $candidate['id'] ?>" class="vote-btn" > <span id="voted1">VOTE</span> </button>
                        </div>
                <?php    }
                }else{
                    echo "Empty";
                }
            ?>
                
            </div> 

            <button onclick="setNextPage()" id="next" disabled >NEXT</button>
            
        </div>
        
        <div id="footer">
            &COPY LAGOS MUSICAL BAND 2019
        </div>
    </header>

    <script type="text/javascript" src="materialize\js\jquery.js"></script>
    <script>
        let selected = 0;
        function voteCandidate(id) {
            const data = {
                id,
                vote: true
            };
            $.post('parser.php', data).then(function(res) {
                console.log(res);
            }).catch(function(error) {
                console.log('error');
            })
        }

        function setNextPage() {
            const data = {
                id: selected,
                currentPage: 'vote-music-dir.php',
                vote: true
            };
            $.post('parser.php', data).then(function(res) {
                console.log(res);
                window.location.reload()
            }).catch(function(error) {
                console.log('error');
            });
        }

        $('.vote-btn').click(function() {
            $('.vote-btn').removeClass('selected').text('VOTE');
            $(this).addClass('selected').text('SELECTED');
            const id = $(this).attr('id');
            setSelected(id);
            $('#next').attr('disabled', false);
        });

        function setSelected(id) {
            selected = id;
        }


    </script>

</body>
</html>