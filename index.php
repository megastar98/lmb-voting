<?php
include 'app/init.php';
$bandit = new Bandit($db_conn);
if (!isset($_SESSION['state_code'])) {
    header('location: login.php');
}

$current_page = $_SESSION['current_page'];
include($current_page);

?>