<?php
include 'app/init.php';
$bandit = new Bandit($db_conn);
if (isset($_POST['bandit_login'])) {
    if (isset($_POST['state_code']) && $_POST['state_code'] != '' && isset($_POST['password']) && $_POST['password'] != ''){
        $email = $_POST['state_code'];
        $password = $_POST['password'];
        $response = $bandit->login($email,$password);
    }else{
        $response = json_encode(array("status"=>0,"message"=>"Please, Fill all fields!"));
    }
    exit($response);
}

if (isset($_POST['vote'])) {
    if (isset($_POST['id'])) {
        $id = $_POST['id'];
        $bandit->incrementVote($id);
        $currentPage = $_POST['currentPage'];
        $_SESSION['current_page'] = $currentPage;   
        exit(true);
    }else{

    }
}

if(isset($_POST['selectPresident'])) {
    $_SESSION['president'] = $_POST['candidateId'];
}

if(isset($_POST['saveVoterCode'])) {
    $voterCode = $_SESSION['state_code'];
    $response = $bandit->saveVoter($voterCode);
    exit($response);
}