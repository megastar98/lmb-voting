<?php  
include 'app/init.php';
$bandit = new Bandit($db_conn);
$candidates = file_get_contents('./candidate-data.json');
$candidates = json_decode($candidates, true);

foreach ($candidates as $candidate) {
    $bandit->addCandidate($candidate);
}

?>