<html lang="en">
<head>
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Raleway&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="index.css"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Thank You</title>
</head>
<body>
    <header>
        <div id="header">
            <nav ìd="menu-bar" class="nav-wrapper transparent no-shadows flat">
                <div id="band_logo">
                    <a href="home.html" class="brand-logo">
                        <img class="logos" src="nysc.png">
                    </a>
                </div>
            </nav>
        </div>
        <!-- position to vote for -->
        <div class="position">
            <h4>Thank You</h4>
        </div>

           </header>

    <script type="text/javascript" src="materialize\js\jquery.js"></script>
    <script>
        let selected = 0;
        function voteCandidate(id) {
            const data = {
                id,
                vote: true
            };
            $.post('parser.php', data).then(function(res) {
                console.log(res);
            }).catch(function(error) {
                console.log('error');
            })
        }

        function setNextPage() {
            const data = {
                id: selected,
                currentPage: 'vote-asst-band-major.php',
                vote: true
            };
            $.post('parser.php', data).then(function(res) {
                console.log(res);
                window.location.reload()
            }).catch(function(error) {
                console.log('error');
            });
        }

        $('.vote-btn').click(function() {
            $('.vote-btn').removeClass('selected').text('VOTE');
            $(this).addClass('selected').text('SELECTED');
            const id = $(this).attr('id');
            setSelected(id);
            $('#next').attr('disabled', false);
        });

        function setSelected(id) {
            selected = id;
        }


    </script>
</body>
</html>