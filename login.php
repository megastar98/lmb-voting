<html lang="en">
<head>
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat|Raleway&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="index.css"/>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
    <header>
        <div id="header">
            <nav ìd="menu-bar" class="nav-wrapper transparent no-shadows flat">
                <div id="band_logo">
                    <a href="home.html" class="brand-logo">
                        <img class="logos" src="nysc.png">
                    </a>
                </div>
                <div id="title">
                    E-VOTING PLATFORM
                </div>
            </nav>
        </div>
        <div id="">
            <div id="instruction">
                Kindly sign in with your state code
            </div>
            <form id="loginForm" action="vote-president.php">

                <div class="card" id="card">
                    <div class="alert white-text">
                    </div>
                    <div class="input-field">
                        <input id="state_code" type="text" class="validate">
                        <label for="state_code">State Code</label>
                    </div>
                    <div class="input-field">
                        <input id="password" type="password" class="validate">
                        <label for="password">Password</label>
                    </div>
                    <button type="submit" id="submit">PROCEED TO VOTE</button>
                </div>
            </form>
        </div>
        
        <div id="footer">
            &COPY LAGOS MUSICAL BAND 2019
        </div>
    </header>

    <script type="text/javascript" src="materialize\js\materialize.min.js"></script>
    <script type="text/javascript" src="materialize\js\jquery.js"></script>
    <script>
          $(document).ready(function() {
          M.updateTextFields();
        });

          $('#loginForm').submit(function(e) {
              e.preventDefault();
              let code = $('#state_code').val();
              let password = $('#password').val();

              let formData = new FormData();
              formData.append('state_code', code);
              formData.append('password', password);
              formData.append('bandit_login', '');

              $.ajax({
                  type: 'POST',
                  url: 'parser.php',
                  data: formData,
                  cache:false,
                  processData: false,
                  contentType: false,
                  success: function (res) {
                      console.log(res);
                      res = JSON.parse(res);
                      let status = res['status'];
                      let message = res['message'];
                      console.log(res);
                      if(status === 1){
                          $('.alert').addClass('green').removeClass('red ').text(message);
                          setTimeout(()=>{
                              window.location = 'index.php';
                          },2000);
                      }else{
                          $('.alert').addClass('red ').removeClass('green').text(message);
                          $('#submit').text('PROCEED TO VOTE').attr('disabled',false);
                      }
                  },
                  error: function (xhr) {
                      alert(xhr.statusText);
                  }
              })
          });
    </script>
</body>
</html>