<?php
/**
 * Created by PhpStorm.
 * User: hybeecodes
 * Date: 3/14/19
 * Time: 6:10 AM
 */

class Bandit extends Master
{
    public function __construct($db_conn)
    {
        parent::__construct($db_conn);
        $this->db = $db_conn;
    }

    public function login($code, $password)
    {
        $password = md5($password);
        $data = "*";
        $table = "bandits";
        $where = "WHERE state_code = '$code' AND password = '$password'";
        $bandit = $this->getData($data, $table, $where);
        if(empty($bandit)){
            $response = array("status"=>0,"message"=>"Sorry, Invalid State Code or Password ");
        }else{
            $_SESSION['state_code'] = $bandit['state_code'];
            $_SESSION['current_page'] = 'vote-president.php';
            $response = array("status"=>1,"message"=>"Login Successful!");
        }
        return json_encode($response);
    }

    public function create($code) {
        $data = "*";
        $table = "bandits";
        $where = "WHERE state_code = '$code'";
        $bandit = $this->getData($data, $table, $where);
        if (!empty($bandit)) {
            return ["status" => 0, "message"=>"Bandit Exists Already"];
        }
        
        $data = [
            "state_code" => $code,
            "password" => md5($this->genBanditPass($code))
        ];
        return $this->insertData($data, $table);
    }

    public function genBanditPass($code)
    {
        $code = intval($code);
        $passArr = [];
        $code *= 5;
        $vowels = ["a", "s", "d", "f"];
        $codeArr = str_split(strval($code));
        for($i = 0; $i < count($codeArr); $i++) {
            array_push($passArr, $codeArr[$i]);
            array_push($passArr, $vowels[$i]);
        }
        $passArr = array_reverse($passArr);
        $password = implode($passArr);
        return $password;
    }

    public function addCandidate($candidate)
    {
        $data = [
            "full_name" => $candidate['full_name'],
            "batch" => $candidate['batch'],
            "image" => $candidate['image'],
            "position_id" => $candidate['position_id']
        ];
        $table = "candidates";
        $candidate = $this->insertData($data, $table);
        $last_id = $this->db->lastInsertId();
        $votingData = $this->createVoteData($last_id);
        if($candidate && $votingData) {
            return ["status" => 1, "message" => "Candidate Created Successfully"];
        }
        return ["status" => 0, "message" => "Unable to create Candidate"];
    }

    public function incrementVote($candidateId)
    {
        $sql = "UPDATE votes SET vote = vote+1 WHERE candidate_id = $candidateId";
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
    }

    public function createVoteData($candidate)
    {
        $data = [
            "candidate_id" => $candidate,
            "vote" => 0
        ];
        $table = "votes";
        $vote = $this->insertData($data, $table);
        if(empty($vote)) {
            return false;
        }
        return true;
    }

    public function getAllPositions()
    {
        $data = "*";
        $table = "positions";
        $positions = $this->getAllData($data, $table);

        return empty($positions)? [] : $positions;
    }

    public function getCandidates($positionId)
    {
        $data = "*";
        $table = "candidates";
        $where = "WHERE position_id = $positionId";
        $candidates = $this->getAllData($data, $table, $where);
        return $candidates? $candidates : [];
    }
}